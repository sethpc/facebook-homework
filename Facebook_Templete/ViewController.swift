//
//  ViewController.swift
//  Facebook_Templete
//
//  Created by KSHRD_PP_09 on 12/3/18.
//  Copyright © 2018 KSHRD_PP_09. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{
    
    
    
    @IBOutlet weak var tableViewDisplay: UITableView!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Status_templete", for: indexPath)
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewDisplay.register(UINib(nibName: "Facebook_templete", bundle: nil), forCellReuseIdentifier: "Status_templete")
        // Do any additional setup after loading the view, typically from a nib.
    }


}

